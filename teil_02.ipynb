{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Datentypen in Python\n",
    "\n",
    "## Inhalt dieses Kapitels\n",
    "In diesem Kapitel schauen wir uns die wichtigsten Datentypen in Python an\n",
    "- Zahlen (`number`, `int`, `float`, `rational`)\n",
    "- Zeichenketten (`str`)\n",
    "- Wahrheitswerte\n",
    "- Listen & Tupel\n",
    "- Dictionaries\n",
    "\n",
    "## Zahlen in Python\n",
    "Im ersten Kapitel haben wir uns mit der Berechnung des $\\mathsf{ggT}$ zweier natürlicher Zahlen beschäftigt.\n",
    "Tatsächlich sich *ganze Zahlen* der wichtigste Datentyp in der Informatik.\n",
    "\n",
    "In Python kann man über die Funktion `type` sehen, welchen Typ ein Wert bzw. der Inhalt einer Variablen hat:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "int"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "int"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = 1\n",
    "type(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der Typ `int` steht dabei als Kürzel für *integer*, also eine ganze Zahl.\n",
    "Ganze Zahlen werden in einem Computer als Bitfolge im Binärsystem dargestellt. In vielen Programmiersprachen muss man beim Anlegen einer Integer-Variablen angeben, wie groß diese sein soll – etwa 32 Bit oder 64 Bit – und daraus ergeben sich die größten bzw. kleinsten darstellbaren ganzen Zahlen ($2^{n-1}$ bzw. $-2^{n-1} + 1$ bei $n$ Bit). Das Bit \"ganz links\" gibt dabei das Vorzeichen an. Details zur Darstellung von Integer im Comuter finden Sie auf \n",
    "[Wikipedia](https://de.wikipedia.org/wiki/Integer_(Datentyp)).\n",
    "\n",
    "In Python ist der Typ `int` nur durch den verfügbaren Arbeitsspeicher begrenzt – Python kann im Prinzip beliebig große oder kleine ganze Zahlen darstellen.\n",
    "\n",
    "### Nicht-ganze Zahlen in Python\n",
    "\n",
    "#### Gleitkommazahlen: Der Typ `float`\n",
    "\n",
    "Bei der Division zweier ganzer Zahlen erhält man einen Bruch. Standardmäßig haben \"nicht ganze\" Zahlen in Python den Typ `float`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "float"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(1/2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In der Mathematik kennt man als wichtigste Erweiterungen der ganzen Zahlen einerseits die *reellen Zahlen* und andererseits die *rationalen Zahlen*.\n",
    "Die schlechte Nachricht ist, dass ein Computer reelle Zahlen nicht darstellen kann. \n",
    "Das Problem ist, dass Zahlen wie $\\sqrt{2}$, $\\pi$ oder der goldene Schnitt $\\Phi = \\frac{1 + \\sqrt{5}}{2}$ im Dezimal- oder Binärsystem unendlich viele Stellen haben, die man nicht alle hinschreiben oder abspeichern kann.\n",
    "\n",
    "Der Datentyp, der den reellen Zahlen am nächsten kommt, sind sogenannte **Gleitkommazahlen** oder `float`.\n",
    "Aus der Schule kennen Sie wahrscheinlich die sogenannte \"wissenschaftliche Notation\" für Zahlen. So kann man z.B. die Lichtgeschwindigkeit im Vakuum $c$ schreiben als\n",
    "$$\n",
    "c = 2.99792458 \\cdot 10^8 \\frac{m}{s}\n",
    "$$\n",
    "\n",
    "Entsprechend kann man jede reelle Zahl $x$ näherungsweise in der Form\n",
    "$$\n",
    "x = s \\cdot (1 + M) \\cdot 2^{E-B}\n",
    "$$\n",
    "schreiben, wobei \n",
    "- $s$ das *Vorzeichen* (1 Bit), \n",
    "- $M$ die *Mantisse*, eine Zahl $0 \\leq M < 1$ die in Binärdarstellung mit $p$ Bits gespeichert wird, und\n",
    "- $e = E-B$ der *Exponent* ist, wobei $B$, der *Bias*, eine Konstante ist und $E$ mit $r$ Bits gespeichert wird. Mit dem Bias kann man festlegen, wie groß die größte Zahl bzw. wie klein die kleinste positive Zahl ist, die man darstellen kann.  \n",
    "\n",
    "In der Praxis muss man sich entscheiden, wie man die Bits eines *Speicherwortes* im Computer – üblicher Weise 32 oder 64 – auf Mantisse und Exponent aufteilt und welchen Wert man für den Bias wählt.\n",
    "Dies ist in der Norm [IEEE 754](https://de.wikipedia.org/wiki/IEEE_754) festgelegt, nach der alle heute üblichen CPUs arbeiten:\n",
    "\n",
    "Typ     |Größe   |Exponent ($r$)|Mantisse ($p$)|Werte des Exponenten ($e$)|Biaswert ($B$) | Dezimalstellen\n",
    "--------|--------|--------------|--------------|--------------------------|---------------|----------------\n",
    " single | 32 Bit |  8 Bit       |       23 Bit |   −126 ≤ $e$ ≤ 127       |  127          |  7 - 8\n",
    " double | 64 Bit | 11 Bit       |       52 Bit |   −1022 ≤ e ≤ 1023       | 1023          | 15 - 16\n",
    "\n",
    "Python verwendet auf fast allen Platformen den Typ *double* gemäß obiger Tabelle für `float`.\n",
    "\n",
    "Wichtig ist, dass sich viele Zahlen – etwa $1/10$ – als `float` nicht exakt darstellen lassen. Dadurch entstehen **Rundungsfehler**, die sich in bestimmten Fällen auch \"aufschaukeln\" können (ein pathologisches Beispiel, die *Muller Rekursion*, gibt es als Bonusmaterial)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Dezimalzahlen: Der Typ `Decimal`\n",
    "\n",
    "Ein grundsätzlicher Nachteil des Datentyps `float` ist, dass er \"glatte\" Dezimalzahlen wie 0,01 nicht darstellen kann. Da insbesondere Währungen mit dem Dezimalsystem funktionieren, können die wenigsten Geldbeträge als `float` exakt abgebildet werden.\n",
    "Für betriebswirtschaftliche oder finanzmathematische Anwendugen kann das aufgrund der Rundungsfehler ein Problem sein. Schon die \"historische\" Programmiersprache COBOL (Common Business Oriented Language) kannte daher den Datentyp *Decimal*, der Gleit- und Festkommaarithmetik im Dezimalsystem ermöglicht. \n",
    "![Dilbert zu COBOL](https://assets.amuniversal.com/cbf69860a084012f2fe600163e41dd5b)\n",
    "\n",
    "Tatsächlich ist die Migration der Dezimalarithmetik eines der größeren Probleme bei der Migration von COBOL-Programmen in modernere Programmiersprachen wie Java (s. Artikel [Is COBOL Holding You Hostage with Math?](https://medium.com/@bellmar/is-cobol-holding-you-hostage-with-math-5498c0eb428b)).\n",
    "\n",
    "\n",
    "In Python gibt es dazu den Datentypen `Decimal`. Damit kann man z.B. den Unterschied zwischen dem `float` 0.1 und 1/10 berechnen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Decimal('5.551115123125782702118158340E-18')"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from decimal import Decimal\n",
    "\n",
    "Decimal(0.1) - (Decimal(1)/Decimal(10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zur Erläuterung: Mit \n",
    "```Python\n",
    "from decimal import Decimal\n",
    "```\n",
    "importieren wir die Klasse (dazu später mehr) `Decimal` aus dem Paket `decimal`.\n",
    "\n",
    "Wie die Klasse funktioniert, zeigt die (hier sehr umfangreiche) Hilfe. Wichtig ist, dass man mit `Decimal(zahl)` eine der `zahl` entsprechende Dezimalzahl erzeugt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "help(Decimal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exakte Bruchrechnung: Der Typ `Fraction`\n",
    "\n",
    "Bleibt man bei den Grundrechenarten `+`, `-`, `*` und `/`, dann sind *rationale Zahlen* hilfreich, da sie das exakte Ergebnis Darstellen können. Hierzu gibt es in Python den \n",
    "Datentypen **Fraction**, den man mit `Fraction(zähler, nenner)` initialisiert:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Fraction(37, 14)"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from fractions import Fraction\n",
    "\n",
    "Fraction(156, 56) - Fraction(1, 7)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mit `Fraction` sind im Prinzip beliebig genaue Brechnungen mit Brüchen möglich. Bei der oben erwähnten *Muller Rekursion* kann man diesen Vorteil sehr gut sehen – im Gegensatz zu `float` oder `Decimal` liefert `Fraction` das korrekte Ergebnis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wahrheitswerte\n",
    "\n",
    "Der Typ `bool` kennt nur die beiden Werte `True`und `False`. Wichtig sind Wahrheitswerte vor allem für bedingte Anweisungen wie `if` und `while`. In der Praxis tauchen Wahrheitswerte vor allem als Ergebnisse von Vergleichen auf:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "bool"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = 2\n",
    "\n",
    "# Der folgende Vergleich mit == liefert einen Wahrheitswert\n",
    "type(x == 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Zeichenketten (Texte)\n",
    "\n",
    "Neben Zahlen sind Texte der zweite wichtige Datentyp in jeder Programmiersprache. Dabei werden Texte als folge einzelner Zeichen betrachtet.\n",
    "Aber wie speichert man Zeichen im Computer?\n",
    "\n",
    "Gängige westliche Sprachen benutzen das lateinische Alphabet mit 26 Zeichen ('A' bis 'Z') in den Varianten Groß- und Kleinschreibung. Dazu kommen \n",
    "10 Zigffern, einige Sonderzeichen sowie in vielen Sprachen Umlaute und weitere \"Spezialzeichen\". Durch Zählen stellt man fest, dass man 64 Zeichen (entsprechend 6 Bit) nicht auskommt. \n",
    "\n",
    "Lange Zeit hat man den \n",
    "[ASCII Code](https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange) (ASCII) verwendet, der 128 Zeichen enthält (7 Bit), \n",
    "und Erweiterungen mit 256 Zeichen (8 Bit) nach der Norm [ISO 8859](https://de.wikipedia.org/wiki/ISO_8859). \n",
    "\n",
    "Das Problem dieser Kodierungen ist, dass man mit ihnen in einem Dokument nicht sowohl deutsche Sonderzeichen (ISO 8859-15) als auch \n",
    "z.B. griechische Zeichen (ISO 8859-7) verwenden kann. Ganz zu schweigen davon, dass chinesische Zeichen sich nicht mit mit 8 Bit kodieren lassen.\n",
    "Dazu verwendet man den [Unicode Standard](https://de.wikipedia.org/wiki/Unicode), der in der aktuellen Version 1.114.112 verschiedene Zeichen darstellen kann.\n",
    "\n",
    "Mit Unicode kann man so problemlos **你好** und **καλημέρα** in einen deutschen Text einbauen. Um Speicherplatz zu sparen, verwendet man  Kodierungen wie [UTF-8](https://de.wikipedia.org/wiki/UTF-8) oder [UTF-16](https://de.wikipedia.org/wiki/UTF-16), die eine Darstellung mit variabler Länge verwenden. So passen \"normale\" ASCII-Zeichen in der UTF-8-Darstellung in ein Byte, währende deutsche Umlaute zwei Bytes belegen.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Zeichenketten in Python: Der Typ `str`\n",
    "\n",
    "In Python schreibt man Zeichenketten so:\n",
    "```Python\n",
    "\"Dies ist ein Text\"\n",
    "\n",
    "'Dies ist auch ein Text'\n",
    "\n",
    "\"\"\"Dies ist ein \n",
    "längerer, mehrzeiliger\n",
    "Text\"\"\"\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Zeichenketten als *Sequenztypen*\n",
    "\n",
    "In Python ist der Typ `str` ein sogenannter *Sequenztyp*. Alle Sequenztypen – neben Strings z.B. auch Listen und Tupel – unterstützen die folgenden Operationen:\n",
    "\n",
    "| Operation              | Ergebnis                                                                |\n",
    "|------------------------|-------------------------------------------------------------------------|\n",
    "| `x in s`               | `True` falls eines der Elemente von `s` gleich `x` ist, sonst `False`   |\n",
    "| `x not in s`           | `False` falls eines der Elemente von `s` gleich `x` ist, sonst `True`   |\n",
    "| `s + t`                | `s` und `t`  aneinandergehängt                                          |\n",
    "| `s * n or n * s`       | `n`-malige Wiederholung von `s`                                         |\n",
    "| `s[i]`                 | `i`tes Element von `s`, wobei `0`das erste Element liefert              |\n",
    "| `s[i:j]`               | Teilfolge von `s` von `i` (einschließlich) bis `j` (ausschließlich)     |\n",
    "| `s[i:j:k]`             | Teilfolge von `s` von `i` bis `j` mit Schrittweite `k`                  |\n",
    "| `len(s)`               | Länge `s`                                                               |\n",
    "| `min(s)`               | Kleinstes Element von `s`                                               |\n",
    "| `max(s)`               | Größtes Element von `s`                                                 |\n",
    "| `s.index(x[, i[, j]])` | Index des ersten Vorkommens von `x` in `s` (von `i` bis `j`)            |\n",
    "| `s.count(x)`           | Anzahl der Vorkommen von `x` in `s`                                     |\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hier ein paar Beispiele für Operationen mit Zeichenketten:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello world! \n"
     ]
    }
   ],
   "source": [
    "h = \"Hello\"\n",
    "w = \"world\"\n",
    "\n",
    "greeting = h + \" \" + w + \"! \"\n",
    "print(greeting)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Etwas gewöhnungsbedürftig -- aber irgenwie logisch -- ist die Multiplikation von Zeichenketten mit einer (ganzen) Zahl:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello world! Hello world! Hello world! Hello world! \n"
     ]
    }
   ],
   "source": [
    "greeting *= 2\n",
    "print(greeting)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Listen und Tupel\n",
    "\n",
    "Listen und Tupel sind zwei weitere wichtige *Sequenztypen*, die wir bereits in den Übungsaufgaben kennengelernt haben. \n",
    "\n",
    "*Listen* sind geordnete Sammlungen von Objekten, die man wie folgt schreibt:\n",
    "```Python\n",
    "liste = [ 1, 2.5, 'Hallo', [ 1, 2 ] ]\n",
    "```\n",
    "\n",
    "Wie man hier sieht, gibt es im Gegensatz zu vielen anderen Sprachen keine Einschränkung, welchen Typ die Elemente der Liste haben können.\n",
    "`liste[0]` hat in obigem Beispiel den Typ `int`, während `liste[3]` selber wieder eine Liste (`list`) ist.\n",
    "\n",
    "Listen sind *veränderbar*, d.h. es lassen sich Elemente ersetzen, einfügen oder löschen. \n",
    "Veränderbare Sequenztypen unterstützen neben den oben aufgeführten die folgenden Operationen:\n",
    "\n",
    "| Operation                   | Ergebnis                                                                     |\n",
    "|-----------------------------|------------------------------------------------------------------------------|\n",
    "| `s[i] = x`                  | Das `i`te Element von `s` wird durch `x` ersetzt.                            |\n",
    "| `s[i:j] = t`                | Der Abschnitt von `s` vom Element `i` bis `j` wird durch `t` ersetzt.        |\n",
    "| `del s[i:j]`                | Der Abschnitt von `s` vom Element `i` bis `j` wird gelöscht.                 |\n",
    "| `s.append(x)`               | Hängt `x` am Ende von `s` an.                                                |\n",
    "| `s.clear()`                 | Entfernt alle Elemente aus `s`.                                              |\n",
    "| `s.copy()`                  | Erstellt eine Kopie von `s` (wobei die Element selbst nicht kopiert werden). |\n",
    "| `s.extend(t)` oder `s += t` | Hängt `t` an `s` an.                                                         |\n",
    "| `s *= n`                    | Ersetzt `s` durch die `n`-fache Kopie von `s`.                               |\n",
    "| `s.insert(i, x)`            | Fügt `x` an Position `i` in `s` ein (restliche Elemente werden verschoben).  |\n",
    "| `s.pop(i)`                  | Löscht das `i`-te Element aus `s` und gibt es zurück.                        |\n",
    "| `s.remove(x)`               | Löscht das erste Element aus `s`, das gleich `x` ist.                        |\n",
    "| `s.reverse()`               | Kehrt die Reihenfolge der Elemente in `s` um                                 |\n",
    "\n",
    "Listen lassen sich gut dazu verwenden, Objekte zu speichern.\n",
    "\n",
    "*Tupel* unterscheiden sich von Listen dadurch, dass sie *nicht veränderbar* sind. Instanzen von Tupeln schreibt man wie folgt:\n",
    "```Python\n",
    "t = (2, 3, 5, 7, 11)\n",
    "```\n",
    "\n",
    "**Achtung:** Nicht veränderbar heißt, dass das Tupel selbst nicht veränderbar ist – die Elemente selbst können sich ggf. ändern, wie das folgende Beispiel zeigt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, [1, 2])\n",
      "(1, [1, 2, 1, 2])\n"
     ]
    }
   ],
   "source": [
    "x = 1\n",
    "y = [ 1, 2 ]\n",
    "t = (x, y)\n",
    "print(t)\n",
    "x *= 2\n",
    "y *= 2\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Das erste Element des Tupels ändert sich nicht, da die Zahl 1 selbst nicht änderbar ist. \n",
    "Im Gegensatz dazu kann sich das zweite Element – selbst eine Liste – ändern."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Listen und Tupel erzeugen\n",
    "\n",
    "Listen und Tupel kann man auch aus anderen Sequenztypen ezeugen, wie ewa einer Zeichenkette:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['H', 'a', 'l', 'l', 'o']"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "list('Hallo')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "('H', 'a', 'l', 'l', 'o')"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tuple('Hallo')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comprehensions\n",
    "\n",
    "Eine weitere Art, Listen und Tupel zu erzeugen, sind sogenannte *Comprehensions*.\n",
    "\n",
    "Mit \n",
    "```Python\n",
    "[ x*x for x in range(1, 11) ]\n",
    "```\n",
    "erzeugt man z.B. die Liste der ersten 10 Quadratzahlen, und mit\n",
    "```Python\n",
    "[ x for x in range(1, 100) if x % 2 == 0 and x % 3 == 0 ]\n",
    "```\n",
    "erzeugt man die Liste aller Zahlen kleiner 100, die durch 2 und durch 3 teilbar sind."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "[ x for x in range(1, 100) if x % 2 == 0 and x % 3 == 0 ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comprehensions sind eine sehr elegante Möglichkeit, Listen oder Mengen von Elementen zu bilden, die bestimmte Bedingungen erfüllen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dictionaries\n",
    "\n",
    "Betrachtet man Listen und Tupel vom mathematischen Standpunkt aus, so handelt es sich um Zuordnungen einer Zahl, dem *Index*, zu einem Element.\n",
    "\n",
    "Oft ist es praktisch, als Index nicht Zahlen, sondern andere Objekte zu verwenden. Zum Beispiel kann es praktisch sein, `\"Christian Gawron\"` die Emailadresse `\"gawron.christian@fh-swf.de\"` zuzuordnen.\n",
    "Im Buch *Algorithmen kapieren* wird das Funktionsprinzip einer solchen Datenstruktur im Kapitel 5 (*Hashtabellen*) beschrieben.\n",
    "Statt von Indizes spricht man bei Hashtabellen von *Schlüsseln* – Hashtabellen sind Mengen von Schlüssel-Wert-Paaren, die einen Zugriff auf die Werte in konstanter Zeit ermöglichen.\n",
    "\n",
    "In Python gibt es dafür den Datentyp `dict`, den man wie folgt schreibt:\n",
    "```Python\n",
    "emails = { \"Christian Gawron\": \"gawron.christian@fh-swf.de\", \"Uwe Gogolin\": \"gogolin.uwe@fh-swf.de\" }\n",
    "```\n",
    "\n",
    "Während die Werte in einem `dict` beliebige Typen haben können, müssen die Schlüssel einen Typen haben, der *hashable* ist. Von den Typen, die wir bisher kennengelernt haben, ist lediglich `list` nicht *hashable* (Warum wohl nicht?)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
